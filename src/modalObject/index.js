let modalObj = [
    {
       id: 1,
       header: "Do you want to add this product to your basket?",
       text: 'Clicking "ok" will add this product to your basket',
       closeButton: true,
    },
    {
       id: 2,
       header: "Do you want to close this modal?",
       text: "This is test second modal",
       closeButton: false,
    }
 
]

export default modalObj