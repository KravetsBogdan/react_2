import React, { Component } from 'react';
import CardList from '../component/CardList';
import Header from '../component/Header';

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shoppingCard: [],
      favouriteCard: [],
    };
  }

  handleShoppingCardChange = (newShoppingCard) => {
    this.setState({ shoppingCard: newShoppingCard });
  };

  handleFavouriteCardChange= (newFavouriteCard) => {
    this.setState({favouriteCard: newFavouriteCard});
  }
  render() {
    return (
      <>
        <Header shoppingCard={this.state.shoppingCard} favouriteCard={this.state.favouriteCard}/>
        <CardList
          shoppingCard={this.state.shoppingCard}
          onShoppingCardChange={this.handleShoppingCardChange}
          onFavouriteCardChange={this.handleFavouriteCardChange}
        />
      </>
    );
  }
}

