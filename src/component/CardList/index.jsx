import React, { Component } from 'react';
import CardItem from '../CardItem';
import style from './style.module.scss';
import modalObj from '../../modalObject';
import Modal from '../Modal';
import Button from '../Button';

export default class CardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shoppingCard: [],
      favCards: [],
      allCards: [],
      openModal: false,
      modalInfo: {
        text: '',
        header: '',
        closeButton: '',
        firstBtn: '',
        secondBtn: '',
        name: '',
        price: '',
        color: '',
        id: '',
        img: '',
        article: '',
      },
    };
  }

  componentDidMount() {
    fetch('./mock/items.json')
      .then((res) => res.json())
      .then((data) => {
        this.setState({ allCards: data });
      });

    const savedShoppingCard = localStorage.getItem('basket');
    if (savedShoppingCard) {
      this.setState({ shoppingCard: JSON.parse(savedShoppingCard) });
    }

    const savedFavouriteCards = localStorage.getItem('Favourite');
    if (savedFavouriteCards) {
      this.setState({ favCards: JSON.parse(savedFavouriteCards) });
    }
  }
  openModal = (e, name, price, color, id, img, article) => {
    const modalId = parseInt(e.currentTarget.dataset.id);
    const modal = modalObj.find((item) => item.id === modalId);
    if (modal) {
      this.setState({
        modalInfo: {
          ...modal,
          name,
          price,
          color,
          id,
          img,
          article,
        },
        openModal: true,
      });
    }
  };

  handleAddToCart = () => {
    const {
      name,
      price,
      color,
      id,
      img,
      article,
    } = this.state.modalInfo;

    this.setState(
      (prevState) => ({
        shoppingCard: [
          ...prevState.shoppingCard,
          { name, price, color, id, img, article },
        ],
        openModal: false,
      }),
      () => {
        localStorage.setItem(
          'basket',
          JSON.stringify(this.state.shoppingCard)
        );

        this.props.onShoppingCardChange(this.state.shoppingCard);
      }
    );
  };

  handleClickModal = (e) => {
    if (
      e.target.id === 'cancel' ||
      e.target.id === 'ok' ||
      e.target.className === 'backgroundModal' ||
      e.target.className === 'closeModal'
    ) {
      this.setState({ openModal: false });
    }
  };

  favHandleClick = (name, price, color, id, img, article) => {
    this.setState((prevState) => {
      const { favCards } = prevState;
      const index = favCards.findIndex((item) => item.id === id);
  
      if (index !== -1) {
        const updatedFavCards = [...favCards];
        updatedFavCards.splice(index, 1);
        return { favCards: updatedFavCards };
      } else {
        const newItem = { name, price, color, id, img, article };
        return { favCards: [...favCards, newItem] };
      }
    }, () => {
      localStorage.setItem('Favourite', JSON.stringify(this.state.favCards));

      this.props.onFavouriteCardChange(this.state.favCards);
    });
  };

  render() {
    const newData = this.state.allCards;
    return (
      <div className={style.cardWrapper}>
        {newData.map(({ name, price, color, article, img, id }) => {
          return (
            <CardItem
              getCard={(e) =>{
                this.openModal(e, name, price, color, id, img, article)}
              }
              key={id}
              id={id}
              name={name}
              price={price}
              img={img}
              article={article}
              color={color}
              onClick={() => this.favHandleClick(name, price, color, id, img, article)}
              isFavorite={this.state.favCards.some((item) => item.id === id)}
            />
          );
        })}
        {this.state.openModal && (
          <Modal
            close={this.handleClickModal}
            closeModal={this.handleClickModal}
            closeButton = {this.state.modalInfo.closeButton}
            title={this.state.modalInfo.header}
            text={this.state.modalInfo.text}
            actions={
              <>
                <Button
                  onClick={this.handleAddToCart}
                  btnText="OK"
                  bgColor="#a00000"
                  color="white"
                  id="ok"
                />
                <Button
                  btnText="Cancel"
                  bgColor="#a00000"
                  color="white"
                  id="cancel"
                />
              </>
            }
          />
        )}
      </div>
    );
  }
}
