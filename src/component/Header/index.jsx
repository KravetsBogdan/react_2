import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import style from './style.module.scss';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basketCount: 0,
      favArr: 0,
    };
  }

  componentDidMount() {
    const dataBasket = localStorage.getItem('basket');
    if (dataBasket) {
      const basket = JSON.parse(dataBasket);
      this.setState({ basketCount: basket.length });
    }

    const dataFavourite = localStorage.getItem('Favourite');
    if (dataFavourite) {
      const favourite = JSON.parse(dataFavourite);
      this.setState({ favArr: favourite.length });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.shoppingCard !== this.props.shoppingCard) {
      const { shoppingCard } = this.props;
      const basketCount = shoppingCard ? shoppingCard.length : 0;
      this.setState({ basketCount });
    }

    if(prevProps.favouriteCard !== this.props.favouriteCard){
      const {favouriteCard} = this.props;
      const favArr = favouriteCard ? favouriteCard.length : 0;
      this.setState({favArr})
    }
    
  }

  render() {
    const { basketCount, favArr } = this.state;
    

    return (
      <div className={style.header}>
          <button type="button" className={`btn btn-primary ${style.card}`}>
            Cart <span className="badge text-bg-secondary">{basketCount}</span>
          </button>
          <button type="button" className={`btn btn-primary ${style.yellow}`}>
            Favourite <span className="badge text-bg-secondary">{favArr}</span>
          </button>
      </div>
    );
  }
}








