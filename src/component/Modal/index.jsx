import { Component } from 'react';
import './style.scss'

export default class Modal extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
      <>
      <div className='backgroundModal' onClick={this.props.closeModal}>
        <div className='basicModal' style={{backgroundColor: this.props.bgColor}}>
          <div className="headerModal">
            <p className="titleModal">{this.props.title}</p>
            {this.props.closeButton &&  <button onClick={this.props.close} id = {this.props.closeId} className="closeModal">X</button>}
          </div>
          <p className="textModal">{this.props.text}</p>
          <div className="modalBtn">
            {this.props.actions}
          </div>
        </div>
      </div>
      </>
    )
  }
}
