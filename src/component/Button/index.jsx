import { Component } from 'react';
import './style.scss';

export default class Button extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
        <button className='buttons' id = {this.props.id} data-id = {this.props.modalId} onClick={this.props.onClick}>{this.props.btnText}</button>
    )
  }
}
