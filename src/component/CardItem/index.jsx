import React, { Component } from 'react'
import style from './style.module.scss';
import Button from '../Button'



export default class CardItem extends Component {
    constructor(props) {
      super(props)
    };
  render() {
    return (
        <div className={style.cardHolder} id={this.props.id}>
            <img className={style.cardImage} src={this.props.img} alt={this.props.name} width={200} height={150}/>
            <p>{this.props.name}</p>
            <p>Price: <span>{this.props.price}</span>UAH</p>
            <p>Article: <span>{this.props.article}</span></p>
            <p>Color: <span>{this.props.color}</span></p>
            <div className={style.btnHolder}>
              <Button modalId={1} btnText={'Add to bascet'} onClick={this.props.getCard}/>
              <svg onClick={this.props.onClick} className="fav-btn" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24">
               <path fill={this.props.isFavorite ? '#FF6600' : '#000000'} d="M12,2.6L9.3,8.5l-6.1,0.6c-0.89,0.09-1.25,1.19-0.61,1.8L7.5,15l-2.2,6c-0.32,0.86,0.74,1.58,1.45,1.06L12,18.4l5.85,3.66c0.72,0.45,1.77-0.2,1.45-1.06l-2.2-6l4.98-4.4c0.64-0.57,0.28-1.67-0.61-1.8l-6.1-0.6L12,2.6z" />
               <path d="M0,0h24v24H0V0z" fill="none" />
            </svg>
            </div>
        </div>
    )
  }
}


{/* <svg onClick={(e, id) => favBtnClick(e, id)} className="fav-btn" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24">
               <path fill={props.favourite ? '#FF6600' : '#000000'} d="M12,2.6L9.3,8.5l-6.1,0.6c-0.89,0.09-1.25,1.19-0.61,1.8L7.5,15l-2.2,6c-0.32,0.86,0.74,1.58,1.45,1.06L12,18.4l5.85,3.66c0.72,0.45,1.77-0.2,1.45-1.06l-2.2-6l4.98-4.4c0.64-0.57,0.28-1.67-0.61-1.8l-6.1-0.6L12,2.6z" />
               <path d="M0,0h24v24H0V0z" fill="none" />
            </svg> */}

//fill змінювати тут

