import { Component } from 'react'
import {Home} from './pages';

export default class App extends Component {
  
  render() {
    return (
      <>
        <Home />
      </>
    );
  }
}
